update:
	go get -u github.com/valyala/quicktemplate/qtc

generate:
	go generate

install:
	go install

all: update generate install
