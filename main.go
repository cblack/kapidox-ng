//go:generate qtc -dir=app/templates
package main

import "kapidox-ng/app"

func main() {
	app.Main()
}
