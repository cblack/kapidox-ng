// Code generated by qtc from "base.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

//line app/templates/base.qtpl:1
package templates

//line app/templates/base.qtpl:1
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line app/templates/base.qtpl:1
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line app/templates/base.qtpl:3
type Page interface {
//line app/templates/base.qtpl:3
	Title() string
//line app/templates/base.qtpl:3
	StreamTitle(qw422016 *qt422016.Writer)
//line app/templates/base.qtpl:3
	WriteTitle(qq422016 qtio422016.Writer)
//line app/templates/base.qtpl:3
	Body() string
//line app/templates/base.qtpl:3
	StreamBody(qw422016 *qt422016.Writer)
//line app/templates/base.qtpl:3
	WriteBody(qq422016 qtio422016.Writer)
//line app/templates/base.qtpl:3
}

//line app/templates/base.qtpl:10
func StreamPageTemplate(qw422016 *qt422016.Writer, p Page) {
//line app/templates/base.qtpl:10
	qw422016.N().S(`

<html>
	<head>
		<title>`)
//line app/templates/base.qtpl:14
	p.StreamTitle(qw422016)
//line app/templates/base.qtpl:14
	qw422016.N().S(`</title>
	</head>
	<body>
		`)
//line app/templates/base.qtpl:17
	p.StreamBody(qw422016)
//line app/templates/base.qtpl:17
	qw422016.N().S(`
	</body>
</html>

`)
//line app/templates/base.qtpl:21
}

//line app/templates/base.qtpl:21
func WritePageTemplate(qq422016 qtio422016.Writer, p Page) {
//line app/templates/base.qtpl:21
	qw422016 := qt422016.AcquireWriter(qq422016)
//line app/templates/base.qtpl:21
	StreamPageTemplate(qw422016, p)
//line app/templates/base.qtpl:21
	qt422016.ReleaseWriter(qw422016)
//line app/templates/base.qtpl:21
}

//line app/templates/base.qtpl:21
func PageTemplate(p Page) string {
//line app/templates/base.qtpl:21
	qb422016 := qt422016.AcquireByteBuffer()
//line app/templates/base.qtpl:21
	WritePageTemplate(qb422016, p)
//line app/templates/base.qtpl:21
	qs422016 := string(qb422016.B)
//line app/templates/base.qtpl:21
	qt422016.ReleaseByteBuffer(qb422016)
//line app/templates/base.qtpl:21
	return qs422016
//line app/templates/base.qtpl:21
}

//line app/templates/base.qtpl:23
type BasePage struct{}

//line app/templates/base.qtpl:24
func (p *BasePage) StreamTitle(qw422016 *qt422016.Writer) {
//line app/templates/base.qtpl:24
	qw422016.N().S(`untitled`)
//line app/templates/base.qtpl:24
}

//line app/templates/base.qtpl:24
func (p *BasePage) WriteTitle(qq422016 qtio422016.Writer) {
//line app/templates/base.qtpl:24
	qw422016 := qt422016.AcquireWriter(qq422016)
//line app/templates/base.qtpl:24
	p.StreamTitle(qw422016)
//line app/templates/base.qtpl:24
	qt422016.ReleaseWriter(qw422016)
//line app/templates/base.qtpl:24
}

//line app/templates/base.qtpl:24
func (p *BasePage) Title() string {
//line app/templates/base.qtpl:24
	qb422016 := qt422016.AcquireByteBuffer()
//line app/templates/base.qtpl:24
	p.WriteTitle(qb422016)
//line app/templates/base.qtpl:24
	qs422016 := string(qb422016.B)
//line app/templates/base.qtpl:24
	qt422016.ReleaseByteBuffer(qb422016)
//line app/templates/base.qtpl:24
	return qs422016
//line app/templates/base.qtpl:24
}

//line app/templates/base.qtpl:25
func (p *BasePage) StreamBody(qw422016 *qt422016.Writer) {
//line app/templates/base.qtpl:25
	qw422016.N().S(`uncontented`)
//line app/templates/base.qtpl:25
}

//line app/templates/base.qtpl:25
func (p *BasePage) WriteBody(qq422016 qtio422016.Writer) {
//line app/templates/base.qtpl:25
	qw422016 := qt422016.AcquireWriter(qq422016)
//line app/templates/base.qtpl:25
	p.StreamBody(qw422016)
//line app/templates/base.qtpl:25
	qt422016.ReleaseWriter(qw422016)
//line app/templates/base.qtpl:25
}

//line app/templates/base.qtpl:25
func (p *BasePage) Body() string {
//line app/templates/base.qtpl:25
	qb422016 := qt422016.AcquireByteBuffer()
//line app/templates/base.qtpl:25
	p.WriteBody(qb422016)
//line app/templates/base.qtpl:25
	qs422016 := string(qb422016.B)
//line app/templates/base.qtpl:25
	qt422016.ReleaseByteBuffer(qb422016)
//line app/templates/base.qtpl:25
	return qs422016
//line app/templates/base.qtpl:25
}
