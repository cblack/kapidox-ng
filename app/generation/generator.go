package generation

type Generator interface {
	Generate(data *Context) error
}
