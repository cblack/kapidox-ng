package generation

import "kapidox-ng/app/types"

type Context struct {
	TempDir  string
	Metainfo *types.MetainfoYAML
}
