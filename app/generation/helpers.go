package generation

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func TrimExcess(s string) string {
	return strings.Split(strings.Split(s, "<body>")[1], "</body>")[0]
}

func WalkForHTML(wf func(data []byte, w io.Writer) error) error {
	return filepath.Walk("html", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if !strings.HasSuffix(path, "html") {
			return nil
		}
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return nil
		}
		buf := bytes.Buffer{}

		err = wf(data, &buf)
		if err != nil {
			return err
		}

		return ioutil.WriteFile(path, buf.Bytes(), os.ModePerm)
	})
}
