package generation

import (
	"io"
	"io/ioutil"
	"kapidox-ng/app/templates"
	"os"
	"os/exec"
	"path"
	"strings"
)

type QDocGenerator struct {
}

func (q *QDocGenerator) Prepare(md *Context) error {
	err := ioutil.WriteFile(path.Join(md.TempDir, "global.qdocconf"), []byte(globalQDocconf), os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

func (q *QDocGenerator) Generate(md *Context) error {
	err := q.Prepare(md)
	if err != nil {
		return nil
	}

	cmd := exec.Command("qdoc", ".qdocconf", "--outputdir=html")
	reader := strings.NewReader(globalDoxyfile)

	cmd.Stdin = reader
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return err
	}

	err = WalkForHTML(func(data []byte, w io.Writer) error {
		templates.WritePageTemplate(w, &templates.QDocPage{
			Inner: TrimExcess(string(data)),
		})
		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
