package generation

import (
	"io"
	"kapidox-ng/app/templates"
	"os"
	"os/exec"
	"strings"
)

type DoxygenGenerator struct {
}

func (d *DoxygenGenerator) Generate(data *Context) error {
	cmd := exec.Command("doxygen", "-")
	reader := strings.NewReader(globalDoxyfile)

	cmd.Stdin = reader
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		return err
	}

	err = WalkForHTML(func(data []byte, w io.Writer) error {
		templates.WritePageTemplate(w, &templates.DoxygenPage{
			Inner: TrimExcess(string(data)),
		})
		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
