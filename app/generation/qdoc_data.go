package generation

const globalQDocconf = `
depends += qtcore qtquick qtquickcontrols

sources.fileextensions = "*.cpp *.qdoc *.qml"
headers.fileextensions = "*.h *.ch *.h++ *.hh *.hpp *.hxx"
`
