package app

import (
	"io/ioutil"
	"kapidox-ng/app/generation"
	"kapidox-ng/app/types"
	"os"

	"github.com/alecthomas/repr"
	"github.com/urfave/cli/v2"
)

// Main is the entrypoint to the app
func Main() {
	(&cli.App{
		Name: "kapidox",
		ExitErrHandler: func(context *cli.Context, err error) {
			panic(err)
		},
		Commands: []*cli.Command{
			{
				Name: "dump-info",
				Action: func(c *cli.Context) error {
					mi := types.MetainfoYAML{}
					data, err := ioutil.ReadFile("metainfo.yaml")
					if err != nil {
						return err
					}
					err = mi.Unmarshal(data)

					if err != nil {
						println(err.Error())
						return err
					}

					repr.Println(mi)
					return nil
				},
			},
			{
				Name: "generate",
				Action: func(c *cli.Context) error {
					mi := types.MetainfoYAML{}
					data, err := ioutil.ReadFile("metainfo.yaml")
					if err != nil {
						return err
					}
					err = mi.Unmarshal(data)
					if err != nil {
						return err
					}

					dir, err := ioutil.TempDir("", "example")
					if err != nil {
						return err
					}
					defer os.RemoveAll(dir)

					err = os.Setenv("KAPIDOX_DIR", dir)
					if err != nil {
						return err
					}

					var gen generation.Generator
					if mi.QDoc {
						gen = &generation.QDocGenerator{}
					} else {
						gen = &generation.DoxygenGenerator{}
					}

					ctx := generation.Context{
						TempDir:  dir,
						Metainfo: &mi,
					}

					err = gen.Generate(&ctx)
					return err
				},
			},
		},
	}).Run(os.Args)
}
