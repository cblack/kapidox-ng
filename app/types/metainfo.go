package types

import "gopkg.in/yaml.v3"

type MaintainerList []string

var _ yaml.Unmarshaler = &MaintainerList{}

func (m *MaintainerList) UnmarshalYAML(val *yaml.Node) error {
	var (
		ret  string
		aret []string
	)

	err := val.Decode(&ret)
	if err == nil {
		*m = []string{ret}
		return nil
	}
	err = val.Decode(&aret)
	if err == nil {
		*m = aret
		return nil
	}

	return err
}

// Metadata is the metadata associated with a project
type Metadata struct {
	Maintainers MaintainerList      `yaml:"maintainer"`
	Description string              `yaml:"description"`
	Tier        int                 `yaml:"tier"`
	Type        string              `yaml:"type"`
	PortingAid  bool                `yaml:"portingAid"`
	Deprecated  bool                `yaml:"deprecated"`
	Release     bool                `yaml:"release"`
	MailingList string              `yaml:"mailinglist"`
	CMakeName   string              `yaml:"cmakename"`
	Libraries   []map[string]string `yaml:"libraries"`
	Platforms   []Platform          `yaml:"platforms"`
	QDoc        bool                `yaml:"qdoc"`
}

// GroupData is the group data associated with a project
type GroupData struct {
	Group            string   `yaml:"group"`
	Subgroup         string   `yaml:"subgroup"`
	PublicLib        bool     `yaml:"public_lib"`
	PublicSourceDirs []string `yaml:"public_source_dirs"`
}

// Platform is a platform's data
type Platform struct {
	Name string `yaml:"name"`
	Note string `yaml:"note"`
}

var _ yaml.Unmarshaler = &Platform{}

func (m *Platform) UnmarshalYAML(val *yaml.Node) error {
	var (
		ret  string
		data struct {
			Name string `yaml:"name"`
			Note string `yaml:"note"`
		}
	)

	err := val.Decode(&data)
	if err == nil {
		m.Name = data.Name
		m.Note = data.Note
		return nil
	}
	err = val.Decode(&ret)
	if err == nil {
		m.Name = ret
		return nil
	}

	return err
}

// GroupInfo is the groups defined by a metainfo.yaml
type GroupInfo struct {
	Name            string         `yaml:"name"`
	FancyName       string         `yaml:"fancyname"`
	Maintainer      MaintainerList `yaml:"maintainer"`
	MailingList     string         `yaml:"mailinglist"`
	Platforms       []Platform     `yaml:"platforms"`
	Description     string         `yaml:"description"`
	Logo            string         `yaml:"logo"`
	LongDescription []string       `yaml:"long_description"`
	Subgroups       []struct {
		Name        string `yaml:"name"`
		Order       int    `yaml:"order"`
		Description string `yaml:"description"`
	} `yaml:"subgroups"`
}

// MetainfoYAML is the full metainfo.yaml file
type MetainfoYAML struct {
	Metadata
	GroupData

	GroupInfo GroupInfo `yaml:"group_info"`
}

func (v *MetainfoYAML) Unmarshal(data []byte) error {
	var err error

	err = yaml.Unmarshal(data, &v.Metadata)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(data, &v.GroupData)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(data, v)
	if err != nil {
		return err
	}

	return nil
}
