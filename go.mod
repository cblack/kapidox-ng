module kapidox-ng

go 1.15

require (
	github.com/alecthomas/repr v0.0.0-20210301060118-828286944d6a
	github.com/klauspost/compress v1.11.13 // indirect
	github.com/urfave/cli/v2 v2.3.0
	github.com/valyala/fasthttp v1.23.0 // indirect
	github.com/valyala/quicktemplate v1.6.3
	gopkg.in/yaml.v2 v2.2.3
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
